#!/usr/bin/env python3

from flask import Flask, abort, jsonify, g
from flask import request, render_template
import os, weather

app = Flask(__name__,static_url_path='')
app.secret_key = os.urandom(16)
app.config['weather_api_key'] = open('weather_api_key').read().strip()

@app.before_request
def set_api_key():
	g.weather_api_key = app.config.get('weather_api_key')

@app.route('/widget',methods=['GET'])
def widget_route():
	return render_template('widget.html')

@app.route('/api/<api_dest>/<api_val_0>/<int:api_val_1>',methods=['GET','POST'])
def api_route(api_dest,api_val_0=False,api_val_1=False):
	if api_dest == 'get_temp':
		return jsonify(weather.get_temp(api_val_0,api_val_1))
	## TODO: implement in V2. let jquery do more work by building the conversion formula in browser
	elif api_dest == 'convert_temp':
		init_temp = request.form.get('temp',0)
		to = request.form.get('to','F')
		return jsonify(weather.convert_temps(init_temp,to))
	return jsonify({0})

@app.route('/',methods=['GET','POST'])
def index_route():
	weather.get_temp()
	return render_template('index.html')

if __name__ == "__main__":
    app.run(debug=True)