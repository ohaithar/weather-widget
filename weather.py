#!/usr/bin/env python3

from flask import Flask, g
import requests

API_URL = 'api.openweathermap.org/data/2.5'
DEGREE_SIGN= u'\N{DEGREE SIGN}'

def get_temp(z_code=90210,as_f=False):
	url = "https://{}/weather?zip={}&units=metric&APPID={}".format(API_URL,z_code,g.get('weather_api_key'))
	r = requests.get(url)
	try:
		temp = r.json().get('main',{}).get('temp',-1)
	except:
		temp = 0
	if as_f==1:
		return convert_temps("9/5*{}+32".format(temp))
	return "{}{}C".format(temp,DEGREE_SIGN)

def convert_temps(val,to='F'):
	res = eval(val, {'__builtins__':None})
	return "{}{}{}".format(res,DEGREE_SIGN,to)